# Testing some mermaid graphs

## sequence diagram

```mermaid
   sequenceDiagram
      participant Alice
      participant Bob
      Alice->John: Hello John, how are you?
      loop Healthcheck
          John->John: Fight against hypochondria
      end
      Note right of John: Rational thoughts <br/>prevail...
      John-->Alice: Great!
      John->Bob: How about you?
      Bob-->John: Jolly good!
```

## flowchart

```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```

## class diagram

```mermaid
classDiagram
Class01 <|-- AveryLongClass : Cool
Class03 *-- Class04
Class05 o-- Class06
Class07 .. Class08
Class09 --> C2 : Where am i?
Class09 --* C3
Class09 --|> Class07
Class07 : equals()
Class07 : Object[] elementData
Class01 : size()
Class01 : int chimp
Class01 : int gorilla
Class08 <--> C2: Cool label
```

```mermaid
graph TB %% comments
  %% Entity[Text]
  ID-1(This is <br>block number 1)
  ID-2>Node 2]
  ID-3(Node 3 <br> text)
  %% Entity--Entity
  ID-1---ID-2
  ID-1 --> ID-3
  %% Entity--Text--Entity
  ID-2--Link between 2 and 3---ID-3
  ID-3-->|Action from 3 to 1|ID-1
  ID-3 -- "Action from 3 to 2. p/w: '_-!#$%^&*+=?,\'" --> ID-2
  %% Complex cases
  
  A[(Torus)] -->|Link text| B((Circle))
  ID-1-->ID-2(Some text) %% overriding the 
  B --> C
  C -->|One| D
  A --> B
  D --> E([rounded rectangle])
  C[/Parallelogram/] --> D>Flag]
  %% class/classDef
  classDef main fill:#08f,stroke:#038;
  classDef second fill:#f80,stroke:#830;
  class ID-1 main
  class ID-3,ID-2 second
  %% click
  click ID-1 "https://github.com" "Tooltip text" %% comments
  click ID-2 alert "Tooltip for a callback"
  %% subgraph
  subgraph A subgraph
    ID-4{Node 4}
    ID-5((fa:fa-spinner))
    ID-6(["Node 6 (same #quot;shape#quot;)"])
    ID-4 .-> ID-5
    ID-5 -. Action from 5 to 4 .-> ID-4
    ID-5 ==> ID-6
    ID-6 == Action from 6 to 5 ==> ID-5
  end
```
